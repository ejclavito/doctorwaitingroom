// Patient Object
function Patient(data) {
    // Data
    this.name = data.name;
    this.reason = data.reason;
    this.vsee_id = data.vsee_id;
    this.vsee_id_url = ko.computed(function () {
        return "vsee:" + data.vsee_id;
    });
    this.patient_status = 1;
    this.waiting_time = ko.observable();
    this.uuid = data.uuid;
    this.timestamp = data.timestamp;
}