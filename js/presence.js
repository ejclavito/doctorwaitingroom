// If person leaves or refreshes the window, run the unsubscribe function
onbeforeunload = function() {
    globalUnsubscribe();
    $.ajax({
        // Query to server to unsub sync
        async:false,
        method: "GET",
        url: "https://pubsub.pubnub.com/v2/presence/sub-key/sub-c-8c491e3c-4d8d-11e9-8d60-1a250947a5a3/channel/waiting_room/leave?uuid=" + encodeURIComponent(UniqueID)
    }).done(function(jqXHR, textStatus) {
        console.log( "Request done: " + textStatus );
    }).fail(function( jqXHR, textStatus ) {
        console.log( "Request failed: " + textStatus );
    });
    return null;
}
// Unsubscribe patients from PubNub network
globalUnsubscribe = function () {
    try {
        pubnub.unsubscribe({
            channels: ['waiting_room']
        });
        pubnub.removeListener(listener);
    } catch (err) {
        console.log("Failed to UnSub");
    }
};