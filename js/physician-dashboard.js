var pubnub;
var listener;
var dashboardViewModel;
var UniqueID = 'physician-001'; // A custon UUID is assigned to the physician for easy identification
var timer;

$(document).ready(function () {

    // Configure PubNub
    var pubnub = new PubNub({
        publishKey: 'pub-c-5624345f-6aef-490f-b768-4e930a8294ec',
        subscribeKey: 'sub-c-8c491e3c-4d8d-11e9-8d60-1a250947a5a3',
        uuid: UniqueID,
    });

    // Configure listeners
    listener = pubnub.addListener({
        status: function(statusEvent) {
            if (statusEvent.category === "PNConnectedCategory") {
                // Add state to the physician
                var newState = {
                    name: "An Nguyen",
                    reason: "",
                    vsee_id: "",
                    timestamp: new Date()
                };
                pubnub.setState(
                    {
                        channels: ["waiting_room"],
                        state: newState
                    }
                );
                
                // Update patient list
                hereNow(dashboardViewModel);
            }
        },
        message: function(message) {
        },
        presence: function(response) {
            // Update patient list
            hereNow(dashboardViewModel);
        }
    });

    // Subscribe to the waiting room channel with presence
    pubnub.subscribe({
        channels: ["waiting_room"],
        withPresence: true
    });
    
    function hereNow(dashboardViewModel) {

        /*  This part of the code populates the patient array.
        *   Gets all the subscribers in the channel then filter out the physician
        *   before storing in the array.
        */
        pubnub.hereNow(
            {
                channels: ['waiting_room'],
                includeUUIDs: true,
                includeState: true,
            },
            function(status, response) {

                // Get the list of subscribers and map it to create patient instances
                var mappedPatients = $.map(response.channels.waiting_room.occupants, function(item) { 

                    // Add the uuid to identify the object and filter out the physician
                    if (item.uuid != 'physician-001') {
                        item.state.uuid = item.uuid;
                        return new Patient(item.state);
                    }
                });

                dashboardViewModel.patients(mappedPatients);
            }
        );
    }

    // Dashboard Model
    function DashboardViewModel() {
        // Data
        var self = this;
        self.patients = ko.observableArray([]);

        /**
         * This function sends signal to the channel to which patient is going to call
         */
        self.openvideo = function (uuid) {
            pubnub.publish({
                message: {
                    "sender" : UniqueID,
                    "recepient" : uuid,
                    "message" : "calling",
                },
                channel: 'waiting_room'
            });

            return true;
        }
    };

    // Apply bindings
    dashboardViewModel = new DashboardViewModel();
    ko.applyBindings(dashboardViewModel);

    /**
     * Custom binding for timers
     * This function gets the current timestamp and the sunscriber timestamp
     * to compute the waiting time.
     */
    ko.bindingHandlers.timer = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContent) {
            timer = setInterval(function() { 
                var now = new Date().getTime(); // Get current time
                var countDownData = viewModel.timestamp; // Get subscription timestamp

                // Time calculations for days, hours, minutes and seconds
                var distance = now - countDownData; 
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                // Update the view
                $(element).text(minutes);
            }, 1000);
        }
    };
});