var UniqueID = PubNub.generateUUID(); // Generate UUID
// Configure PubNub
var pubnub = new PubNub({
    publishKey: 'pub-c-5624345f-6aef-490f-b768-4e930a8294ec',
    subscribeKey: 'sub-c-8c491e3c-4d8d-11e9-8d60-1a250947a5a3',
    uuid: UniqueID,
});
var listener;

$(document).ready(function () {
    // Waiting Room Model
    function WaitingRoomViewModel() {
        // Data
        var self = this;
        self.patientName = ko.observable();
        self.reason = ko.observable();
        self.vseeID = ko.observable();
        self.patients = ko.observableArray([]);
        self.call_status = ko.observable("Your provider will be with you shortly");
    
        // Operations
        self.save = function() {
            if ((self.patientName() == undefined) || (self.vseeID() == undefined)) {
                alert('Please fill all required fields.'); 
            } else {
                //Subscribe to the waiting_room channel with presence
                listener = pubnub.addListener({
                    status: function(statusEvent) {
                        if (statusEvent.category === "PNConnectedCategory") {

                            // Define Patient State, data is also used to store patient information
                            var newState = {
                                name: self.patientName(),
                                reason: self.reason(),
                                vsee_id: self.vseeID(), // vsee id displayed in the provider dashboard and vsee call button
                                uuid: UniqueID, // used to identify the patient when clicking the video icon
                                timestamp: new Date().getTime() // used to calculate waiting time
                            };
                            pubnub.setState(
                                {
                                    channels: ['waiting_room'],
                                    state: newState
                                }
                            );

                            // Add to the patient array, used to identify if a patient is in the waiting room
                            self.patients.push(new Patient({ name: self.patientName(),  reason: self.reason(), vsee_id: self.vseeID()}));

                            // Clear previous values
                            self.patientName("");
                            self.reason("");
                            self.vseeID("");
                        }
                    },
                    message: function(messages) {

                        /*  This part of the code is used to identify which text is displayed in the waiting room.
                        *   When the provider clicks the video button, it will broadcast a message in the channel to announce
                        *   that a video call has been initiated.
                        */

                        // Check if the signal is for this patient, then do the evaluation
                        if (messages.message.recepient.uuid == UniqueID) {
                            if (messages.message.message == "calling") {
                                self.call_status("The visit is in progress");
                            }
                        } else {
                            self.call_status("Doctor is currently busy and will attend to you soon");
                        }
                    },
                    presence: function(presenceEvent) {
                        console.log(presenceEvent.action);
                    }
                });

                // Subscribe to the cahnnel
                pubnub.subscribe({
                    channels: ['waiting_room'],
                    withPresence: true
                });
            }
        };

        self.exit = function() {
            // Unsubcribe from the channel
            pubnub.unsubscribe({
                channels: ['waiting_room']
            });
            self.patients.removeAll();
        }
    };

    ko.applyBindings(new WaitingRoomViewModel());
});