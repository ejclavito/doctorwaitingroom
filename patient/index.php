<!doctype html>
    <head>
        <title>Patient Waiting Room</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="css/styles.css">

        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

        <script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">


        <!-- Include the PubNub Library -->
        <script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.21.7.js"></script>

        <script src="js/knockout-3.5.0.js"></script>
        <script src="js/patient.js"></script>
        <script src="js/patient-waitingroom.js"></script>
        <script src="js/presence.js"></script>
    

    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar">
                        <!-- Brand/logo -->
                        <a class="navbar-brand" href="#">
                            <img src="https://i1.wp.com/vsee.com/wp-content/uploads/2015/05/VSee_Lab_Logo.png" alt="logo" style="width:180px;">
                        </a>

                        <!-- Links -->
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="fas fa-desktop"></i> Test Computer</a>
                            </li>
                        </ul>

                    </nav>
                </div>
            </div>

            <hr>

            <h2 class="text-center">Welcome to Code Challenge Waiting Room</h2>
            <p class="text-center">If this is an emergency, please call 911.</p>
            <div class="card" data-bind="visible: patients().length == 0">
                <div class="card-header"><i class="fas fa-video"></i> Talk to An Nguyen</div>
                <div class="card-body">
                    <div class="form-group required">
                        <label for="my_name" class="font-weight-bold control-label">Please fill in your name to proceed </label>
                        <input data-bind="value: patientName" type="text" class="form-control" placeholder="Your Name" id="my_name" required />
                    </div>
                    <div class="form-group">
                        <label for="reason" class="font-weight-bold control-label">Reason for visit <span class="text-muted">(optional)</span></label>
                        <textarea data-bind="value: reason" id="reason" cols="30" rows="10" class="form-control" placeholder="Your reason for visit"></textarea>
                    </div>
                    <div class="form-group required">
                        <label for="vsee_id" class="font-weight-bold control-label">VSee ID </label>
                        <input data-bind="value: vseeID" type="text" class="form-control" placeholder="Your VSee ID for doctor to call" id="vsee_id" required />
                    </div>
                    <button data-bind="click: save" class="btn btn-primary" type="button">Enter Waiting Room</button>
                </div> 
            </div>

            <div class="card" data-bind="visible: patients().length > 0">
                <div class="card-header"><i class="fas fa-clock"></i> Connecting with your provider</div>
                <div class="card-body text-center">
                    <div class="col-md-12">
                        <h3 data-bind="text: call_status">Your provider will be with you shortly</h3>
                    </div>
                    <br>
                    <div class="col-md-12">
                        <button data-bind="click: exit" class="btn btn-primary text-center">Exit Waiting Room</button>
                    </div>
                    <br>
                    <hr>
                    <div class="col-md-12">
                        If you close the video conference by mistake, please click here to relaunch video again.
                    </div>
                </div> 
            </div>
        </div>
    </body>
</html>