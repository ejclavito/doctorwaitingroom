<!doctype html>
<html>
    <head>
        <title>Physician Dashboard</title>
        <link rel="stylesheet" href="../css/styles.css">

        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

        <script src="https://unpkg.com/popper.js/dist/umd/popper.min.js"></script>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

        <!-- Include the PubNub Library -->
        <script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.21.7.js"></script>

        <script src="../js/knockout-3.5.0.js"></script>
        <script src="../js/patient.js"></script>
        <script src="../js/physician-dashboard.js"></script>
        <script src="../js/presence.js"></script>

    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar">
                        <!-- Brand/logo -->
                        <a class="navbar-brand" href="#">
                            <img src="https://i1.wp.com/vsee.com/wp-content/uploads/2015/05/VSee_Lab_Logo.png" alt="logo" style="width:180px;">
                        </a>

                        <!-- Links -->
                        <ul class="nav">
                            <li class="nav-item">
                                <a href="#" class="nav-link"><i class="fas fa-desktop"></i> Test Computer</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#"><i class="fas fa-user"></i> An Nguyen</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Link 1</a>
                                    <a class="dropdown-item" href="#">Link 2</a>
                                    <a class="dropdown-item" href="#">Link 3</a>
                                </div>
                            </li>
                        </ul>

                    </nav>
                </div>
            </div>

            <hr>

            <div class="card">
                <div class="card-header">
                    <span class="float-left"><i class="fas fa-bars"></i> Waiting Room</span>
                    <span class="float-right"><i class="fas fa-user-plus"></i> Invite People</span>
                </div>
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row" data-bind="foreach: patients, visible: patients().length > 0">
                            <div class="col-md-2 text-center">
                                <i class="fas fa-user fa-5x"></i>
                            </div>
                            <div class="col-md-4">
                                <p class="font-weight-bold" data-bind="text: name"></p>
                                <p class="text-muted" data-bind="text: reason"></p>
                                <p class="text-muted" data-bind="text: vsee_id"></p>
                            </div>
                            <div class="col-md-3">
                                <p><i class="fas fa-video" style="color:green;"></i> Online</p>
                                <p><i class="far fa-clock"></i> Waiting: <span data-bind="timer: waiting_time"></span> min</p>
                            </div>
                            <div class="col-md-3">
                                <button class="btn btn-primary"><i class="fas fa-comment"></i></button>
                                <a class="btn btn-primary" data-bind="attr: { href: vsee_id_url}, click: $parent.openvideo.bind(uuid)"><i class="fas fa-video"></i></a>
                                <button class="btn btn-primary"><i class="fas fa-ellipsis-h"></i></button>
                            </div>
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </body>
</html>